<?php

namespace App\Http\Controllers;

use App\User;
use App\Vacancy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SiteController extends Controller
{
    public function index(){

        if(Auth::guest()){
            return redirect('login');
        }
        else{
            if(Auth::user()->role == config('constants.roles.moderator')){
                $vacancys = Vacancy::all()->toArray();
            }else{

                $vacancys = Vacancy::where('employer_id','=',Auth::id())->get()->toArray();
            }
            return view('home',compact('vacancys'));
        }
    }

    public function logout(){
        Auth::logout();

        return redirect('/');
    }
}
