<?php

namespace App\Http\Controllers;

use App\Mail\Mailer;
use App\Mail\ModeratorsMailer;
use App\User;
use App\Vacancy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Mail;

class VacancyController extends Controller
{
    public function showCreate(){
        if(Auth::guest()){
            return redirect('login');
        }
        else{
            if(Auth::user()->role == config('constants.roles.employer')){
                return view('vacancy',['state'=>0]);
            }else{
                return redirect('/');
            }

        }
    }

    public function create(Request $request){
        $newVacancy = new Vacancy();
        $newVacancy->employer_id = Auth::id();
        $newVacancy->title = $request->post('title');
        $newVacancy->description = $request->post('description');
        $newVacancy->email = $request->post('email');
        $newVacancy->state = config('constants.vacancys.moderating');
        $newVacancy->save();

        $name = Auth::user()->name;
        $email = Auth::user()->email;

        Mail::to($email)->send(new Mailer($name,$newVacancy));

        $users = User::where('role','=',config('constants.roles.moderator'))->get();

        foreach ($users as $user) {
            $name = $user->name;
            $email = $user->email;
            Mail::to($email)->send(new ModeratorsMailer($name));
        }
        return redirect('/');
    }

    public function showEdit(Request $request){
        if(Auth::guest()){
            return redirect('login');
        }
        else{
            if(Auth::user()->role == config('constants.roles.employer')){
                $id = Input::get()['id'];
                $vacancy = Vacancy::find($id)->toArray();
                return view('vacancy',['state'=>1,'vacancy'=>$vacancy]);
            }else{
                return redirect('/');
            }

        }
    }

    public function edit(Request $request){
        $id = Input::get('id');

        $vacancy = Vacancy::find($id);
        $vacancy->title = $request->post('title');
        $vacancy->description = $request->post('description');
        $vacancy->email = $request->post('email');
        $vacancy->save();

        return redirect('/');
    }

    public function setState(Request $request){
        $id = $request->get('id');
        $vacancy = Vacancy::find($id);
        if(Input::get('accept')){
            $vacancy->state = config('constants.vacancys.accept');
        }elseif (Input::get('decline')){
            $vacancy->state = config('constants.vacancys.decline');
        }
        $vacancy->save();
        return redirect('/');
    }

    public function sendMail(Request $request){
        $name = Auth::user()->name;
        Mail::to('rasskazov.paul@yandex.ru')->send(new Mailer($name));

        return 'Email was sent';
        /*Mail::send(['text'=>'mail'],['name'=>'Birja'],function($message){
            $message->to('pauletto1234@gmail.com','Hey hey hey')->subject('Test mail');
            $message->from('rasskazov.paul@yandex.ru','Bla bla bla');
        });*/
    }

}
