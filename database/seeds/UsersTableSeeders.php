<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //'name', 'email', 'password','role'
        User::create([
            'name'=>'Moderator',
            'email'=>'pauletto1234@gmail.com',
            'password'=>bcrypt('modermoder'),
            'role'=>config('constants.roles.moderator'),
            'remember_token'=>str_random(26)
        ]);
    }
}
