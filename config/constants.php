<?php
/**
 * Created by PhpStorm.
 * User: rasskazov
 * Date: 9/22/18
 * Time: 1:45 PM
 */
return [
    'roles'=>[
        'admin'=>0,
        'moderator'=>1,
        'employer'=>2,
        'employee'=>3,
    ],
    'vacancys'=>[
        'decline'=>0,
        'moderating'=>1,
        'accept'=>2,
    ],
];