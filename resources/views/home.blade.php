@extends('layouts.rialto')
@section('head')
    <div class="container-fluid header-container">
        <div class="main-header">
            <div class="item">
                <h2>Биржа труда</h2>
            </div>
            <div class="item right">
                <h5>Вы зашли как {{\Illuminate\Support\Facades\Auth::user()->name}}</h5>
                &nbsp;
                &nbsp;
                {{--<a class="btn btn-primary btn-small" href="/logout">Выйти</a>--}}
                <h5>
                    <a href="/logout">Выйти</a>
                </h5>
            </div>
        </div>
    </div>
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-6">
            @foreach($vacancys as $vacancy)
                <div class="card card-vacancy">
                    <div class="card-header bg-dark text-white h5">{{$vacancy['title']}}
                        @if(\Illuminate\Support\Facades\Auth::user()->role == config('constants.role.employer'))
                            <a href="/vacancy/edit?id={{$vacancy['id']}}">Изменить</a>
                        @endif
                    </div>
                    <div class="card-body">
                        <p class="h6">{{$vacancy['description']}}</p>
                        <p class="text-primary">Email: {{$vacancy['email']}}</p>
                    </div>
                    <div class="card-footer card-footer-flex">
                        <div class="item">
                            {{$vacancy['updated_at']}}
                        </div>
                        @switch($vacancy['state'])
                            @case(config('constants.vacancys.decline'))
                                <div class="item">
                                    <div class="btn btn-danger">Отклонена</div>
                                </div>
                            @break

                            @case(config('constants.vacancys.moderating'))
                                <div class="item">
                                    <form method="post" action="/vacancy/state/set">
                                        @csrf
                                        <input type="hidden" name="id" value={{$vacancy['id']}}>

                                        @if(\Illuminate\Support\Facades\Auth::user()->role == config('constants.roles.moderator'))
                                            <button  type="submit" class="btn btn-success" name="accept" value="accept">Одобрить</button>
                                            <button  type="submit" class="btn btn-danger" name="decline" value="decline">Отклонить</button>
                                        @else
                                            <div class="btn btn-warning">Модерируется</div>
                                        @endif
                                    </form>
                                </div>
                            @break

                            @case(config('constants.vacancys.accept'))
                                <div class="item">
                                    <div class="btn btn-success">Опубликована</div>
                                </div>
                            @break
                        @endswitch
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    @if(\Illuminate\Support\Facades\Auth::user()->role == config('constants.roles.employer'))
        <div class="row">
            <div class="col-12">
                <a href="/vacancy/create" class="btn btn-primary">Создать вакансию</a>
            </div>
        </div>
    @endif
</div>
@endsection
