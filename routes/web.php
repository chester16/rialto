<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','SiteController@index');
Route::get('/logout','SiteController@logout');

Route::get('/vacancy/create','VacancyController@showCreate');
Route::post('/vacancy/create','VacancyController@create');
Route::get('/vacancy/edit','VacancyController@showEdit');
Route::post('/vacancy/edit','VacancyController@edit');
Route::post('/vacancy/state/set','VacancyController@setState');

Route::get('/vacancy/sendmail','VacancyController@sendMail');

Auth::routes();

/*Route::get('/home', 'HomeController@index')->name('home');*/
