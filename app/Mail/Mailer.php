<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Mailer extends Mailable
{
    use Queueable, SerializesModels;

    public $name,$newVacancy;


    public function __construct($name,$newVacancy)
    {
        $this->name = $name;
        $this->newVacancy = $newVacancy;
    }

    public function build()
    {
        return $this->view('emails.mailsent',compact($this->name,$this->newVacancy));
    }
}
