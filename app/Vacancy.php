<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vacancy extends Model
{
    protected $table = 'vacancys';

    protected $fillable = [
        'employer_id','title','description','email','state'
    ];
}
