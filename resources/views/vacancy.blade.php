@extends('layouts.rialto')
@section('head')
    <div class="container-fluid header-container">
        <div class="main-header">
            <div class="item">
                <h2>Биржа труда</h2>
            </div>
            <div class="item right">
                <h5>Вы зашли как {{\Illuminate\Support\Facades\Auth::user()->name}}</h5>
                &nbsp;
                &nbsp;
                {{--<a class="btn btn-primary btn-small" href="/logout">Выйти</a>--}}
                <h5>
                    <a href="/logout">Выйти</a>
                </h5>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="container">
        @if($state == 0)
            <div class="card">
                <div class="card-header bg-dark text-white h5">Новая вакансия</div>
                <div class="card-body">
                    <form action="/vacancy/create" method="post">
                        @csrf
                        <div class="form-group row">
                            <label for="title" class="col-sm-4 col-form-label text-md-right">Заголовок</label>
                            <div class="col-md-6">
                                <input id="title" type="text" class="form-control" name="title" value="" required autofocus>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="description" class="col-sm-4 col-form-label text-md-right">Описание</label>
                            <div class="col-md-6">
                                <textarea name="description" id="description" cols="50" rows="4" required></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="email" class="col-sm-4 col-form-label text-md-right">Email</label>
                            <div class="col-md-6">
                                <input id="email" type="text" class="form-control" name="email" value="" required>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Отправить на модерацию
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        @elseif($state == 1)
            <div class="card">
                <div class="card-header bg-dark text-white h5">Новая вакансия</div>
                <div class="card-body">
                    <form action="/vacancy/edit?id={{$vacancy['id']}}" method="post">
                        @csrf
                        <div class="form-group row">
                            <label for="title" class="col-sm-4 col-form-label text-md-right">Заголовок</label>
                            <div class="col-md-6">
                                <input id="title" type="text" class="form-control" name="title" value={{$vacancy['title']}} required autofocus>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="description" class="col-sm-4 col-form-label text-md-right">Описание</label>
                            <div class="col-md-6">
                                <textarea name="description" id="description" cols="50" rows="4" required>{{$vacancy['description']}}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="email" class="col-sm-4 col-form-label text-md-right">Email</label>
                            <div class="col-md-6">
                                <input id="email" type="text" class="form-control" name="email" value={{$vacancy['email']}} required>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Сохранить
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        @endif
    </div>
@endsection